package cd.codegen;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import cd.Config;
import cd.ir.Ast.ClassDecl;
import cd.ir.Symbol.ClassSymbol;
import cd.ir.Symbol.MethodSymbol;
import cd.ir.Symbol.VariableSymbol;

/**
 * Generates the  VTables and sets the offsets for the fields and methods
 * @author Lukas Burkhalter
 *
 */
public class VTableGenerator {
	
	public static final int START_FRAME_OFFSET = 4;
	
	public static final int ALIGNMENT = 4;
	
	public static final int MAIN_FUNC_OFFSET = 4;
	
	public static final int ARRAY_LENGTH_FIELD = 4;
	
	public static final int ARRAY_FIST_ELEM = 8;
	
	private StringBuilder out;
	
	public VTableGenerator() {
		
	}
	
	public String generateVTables(List<? extends ClassDecl> astRoots) {
		out  = new StringBuilder();
		
		emitObjectVTable();
		
		HashMap<String,VTable> classToVtable = createVTablesAndInsatnceTable(astRoots);
		
		for(VTable vts : classToVtable.values()) {
			vts.setOffsetsInSymbols();
			out.append(vts);
		}
		
		return out.toString();
	}
	
	private HashMap<String,VTable> createVTablesAndInsatnceTable(List<? extends ClassDecl> classDecls) {
		HashMap<String,VTable> classToVtable = new HashMap<String,VTable>();
		HashMap<String,ObjectInstanceTable> classToInstance = new HashMap<String,ObjectInstanceTable>();
		
		ArrayList<ClassSymbol> toDO = new ArrayList<ClassSymbol>();
		
		for(ClassDecl cd : classDecls) {
			if(!hasNonObjectSuperClass(cd.sym)) {
				VTable vt = new VTable(cd.sym);
				ObjectInstanceTable oi = new ObjectInstanceTable(cd.sym);
				classToVtable.put(cd.name, vt);
				classToInstance.put(cd.name, oi);
			}
			else {
				toDO.add(cd.sym);
			}
		}
		
		for(ClassSymbol cs : toDO) {
			if(!classToVtable.containsKey(cs.name)) {
				doWorkVTables(classToVtable,cs);
			}
			if(!classToInstance.containsKey(cs.name)) {
				doWorkFields(classToInstance,cs);
			}
		}
		
		return classToVtable;
	}
	
	
	private VTable doWorkVTables(HashMap<String, VTable> classToVtable, ClassSymbol cd) {
		VTable superVtable = null;
		if(!classToVtable.containsKey(cd.superClass.name)) {
			return doWorkVTables(classToVtable,cd.superClass);
		}
		else {
			superVtable = classToVtable.get(cd.superClass.name);
			VTable subVtable = new  VTable(cd , superVtable);
			classToVtable.put(cd.name, subVtable);
			return subVtable;
		}
	}
	
	private ObjectInstanceTable doWorkFields(HashMap<String,ObjectInstanceTable> classToInstance, ClassSymbol cd) {
		ObjectInstanceTable superInstance = null;
		if(!classToInstance.containsKey(cd.superClass.name)) {
			return doWorkFields(classToInstance,cd.superClass);
		}
		else {
			superInstance = classToInstance.get(cd.superClass.name);
			ObjectInstanceTable subInstance = new  ObjectInstanceTable(cd , superInstance);
			classToInstance.put(cd.name, subInstance);
			return subInstance;
		}
	}

	private void emitObjectVTable() {
		out.append(Config.V_TABLE_IDENT).append(Config.UNIV_OBJECT + ":" + "\n");
		out.append(indent).append(String.format("%s %s", Config.DOT_INT, "0x0")).append("\n");
	}
	
	private boolean hasNonObjectSuperClass(ClassSymbol classDecl) {
		return !classDecl.superClass.name.equals(Config.UNIV_OBJECT);
	}
	
	//emit 
	protected static String indent = "  ";
	
	
	private class VTable {
		
		public ArrayList<String> methodNames = new ArrayList<String>();
		public ArrayList<String> classNames = new ArrayList<String>();
		
		public ClassSymbol tableOf;
		
		public VTable(ClassSymbol className) {
			Set<String> methodNames = className.methods.keySet();
			boolean isMain = className.name.equals("Main");
			
			if(isMain) {
				doForMain(methodNames,className.name);
			}
			else {
				for(String name: methodNames) {
					this.methodNames.add(name);
					this.classNames.add(className.name);
				}
			}
			this.tableOf = className;
		}
		
		public void doForMain(Set<String> methodNames,String className) {
			for(String name: methodNames) 
				if(name.equals("main")) {
					this.methodNames.add(name);
					this.classNames.add(className);
					break;
				}
			for(String name: methodNames) {
				if(!name.equals("main")) {
					this.methodNames.add(name);
					this.classNames.add(className);
				}
			}
		}
		
		public VTable(ClassSymbol className, VTable superVT) {
			this.tableOf = className;
			Set<String> methodNames = className.methods.keySet();
			
			for(String mName : superVT.methodNames) {
				if(methodNames.contains(mName)) {
					this.methodNames.add(mName);
					this.classNames.add(className.name);
				}
				else {
					this.methodNames.add(mName);
					this.classNames.add(superVT.classNames.get(superVT.methodNames.indexOf(mName)));
				}
			}
			for(String mName : methodNames) {
				if(!superVT.methodNames.contains(mName)) {
					this.methodNames.add(mName);
					this.classNames.add(className.name);
				}
			}
		}
		
		public void setOffsetsInSymbols() {
			for(MethodSymbol ms: tableOf.methods.values()) {
				ms.vTableOffset =  getOffset(ms.name);
			}
		}
		
		public int getOffset(String mName) {
			int index = methodNames.indexOf(mName);
			if(index==-1)
				return -1;
			else {
				return index*ALIGNMENT+Config.SIZEOF_PTR;
			}
			
		}
		
		public String toString() {
			StringBuilder out = new StringBuilder();
			
			out.append(tableOf.getVTableName() + ":" + "\n");
			
			out.append(indent).append(String.format("%s %s", Config.DOT_INT, Config.V_TABLE_IDENT+this.tableOf.superClass.name)).append("\n");
			
			for(int i=0; i<this.classNames.size();i++)
				out.append(indent).append(String.format("%s %s", Config.DOT_INT, this.classNames.get(i)+"_"+this.methodNames.get(i))).append("\n");
			
			return out.toString();
		}
		
	}
	
	private class ObjectInstanceTable {
		
		public ArrayList<String> fields = new ArrayList<String>();
		
		private ClassSymbol classSymb;
		
		public ObjectInstanceTable(ClassSymbol cs) {
			classSymb=cs;
			for(String f : cs.fields.keySet())
				fields.add(f);
			
			setOffsetsToSymbol();
		}
		
		public ObjectInstanceTable(ClassSymbol cs, ObjectInstanceTable superT) {
			classSymb=cs;
			for(String f : superT.fields)
				fields.add(f);
			for(String f : cs.fields.keySet()) {
				fields.add(f);
			}
			
			setOffsetsToSymbol();
		}
		
		private void setOffsetsToSymbol() {
			for(VariableSymbol vs : classSymb.fields.values())
				vs.instanceOffset = getOffset(vs.name);
			classSymb.totalNumberOfFields = fields.size();
		}
		
		public int getOffset(String mName) {
			int index = fields.indexOf(mName);
			if(index==-1)
				return -1;
			else {
				return index*ALIGNMENT+Config.SIZEOF_PTR;
			}
			
		}
		
	}

	
	
	
	
}
