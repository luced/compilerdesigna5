package cd.codegen;

import java.util.Collection;
import java.util.List;

import cd.ir.Ast.ClassDecl;
import cd.ir.Symbol.MethodSymbol;
import cd.ir.Symbol.VariableSymbol;

/*
 * StackFrame
 * higherAddr
 * 
 * param1
 * param2
 * ..
 * paramX <-- $ebp + 16
 * thisRef
 * returnValue
 * returnAddr
 * old ebp <-- $ebp
 * localVar1 <-- $ebp-4
 * localVar2
 * ..
 * localVarN
 * reg1
 * reg2
 * ..
 * regY <-- $esp
 * 
 * lowerAddr
 */

/**
 * Generates the Frame Offsets in the VariableSymbols
 * @author Lukas Burkhalzer
 *
 */
public class FrameGenerator {

	public static final int THIS_REF_OFFSET = 12;
	
	public static final int RETURN_VALUE_OFFSET = 8;
	
	private static final int START_PARAM_OFFSET = 16;
	
	private static final int START_LOCAL_OFFSET = -4;
	
	public static final int ALIGNMENT = 4;
	
	public FrameGenerator() {
	}
	
	public void setFrameOffsets(List<? extends ClassDecl> astRoots) {
			for(ClassDecl cd : astRoots) {
				for(MethodSymbol methSym : cd.sym.methods.values()) {
					setMethodOffsets(methSym);
				}
			}
	}

	private void setMethodOffsets(MethodSymbol methSym) {
		setParameterOffsets(methSym.parameters); 
		setLocalOffsets(methSym.locals.values());
	}

	private void setLocalOffsets(Collection<VariableSymbol> locals) {
		int curLocalOffset = START_LOCAL_OFFSET;
		
		for(VariableSymbol varSym : locals) {
			varSym.instanceOffset = curLocalOffset;
			curLocalOffset-=ALIGNMENT;
		}	
	}

	private void setParameterOffsets(List<VariableSymbol> parameters) {
		int curParamOffset = START_PARAM_OFFSET;
		curParamOffset += (parameters.size()-1)*ALIGNMENT;
		
		for(VariableSymbol varSym : parameters) {
			varSym.instanceOffset = curParamOffset;
			curParamOffset-=ALIGNMENT;
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
