package cd.optimize;

import java.util.ListIterator;

import cd.flow.Block;
import cd.flow.Block.ContentBlock;
import cd.flow.Block.ExitBlock;
import cd.flow.Block.StartBlock;
import cd.flow.BlockFlowGraph;
import cd.ir.Ast.Assign;
import cd.ir.Ast.BuiltInWrite;
import cd.ir.Ast.BuiltInWriteFloat;
import cd.ir.Ast.BuiltInWriteln;
import cd.ir.Ast.IfElse;
import cd.ir.Ast.MethodCall;
import cd.ir.Ast.MethodDecl;
import cd.ir.Ast.Nop;
import cd.ir.Ast.ReturnStmt;
import cd.ir.Ast.VarDecl;
import cd.ir.Ast.WhileLoop;
import cd.ir.Ast;
import cd.ir.AstVisitor;

public class BlockGenerator extends AstVisitor<ContentBlock,ContentBlock> {
	
	private StartBlock start;
	private ExitBlock exit;
	
	public BlockFlowGraph getBlockFlowGraph(MethodDecl md) {
		start = new StartBlock();
		exit = new ExitBlock();
		ContentBlock returnBlock = visit(md, null);
		if(!returnBlock.equals(start) && returnBlock.body.isEmpty()) {
			for(Block cb : returnBlock.beforeBlocks) {
				cb.afterBlocks.remove(returnBlock);
				cb.afterBlocks.add(exit);
				exit.beforeBlocks.add(cb);
			}
		} else {
			addPointersToEachOther(returnBlock,exit);
		}
		return new BlockFlowGraph(start,exit);
	}
	
	
	
	@Override
	public ContentBlock methodCall(MethodCall ast, ContentBlock arg) {
		arg.body.add(ast);
		return arg;
	}



	@Override
	public ContentBlock visitChildren(Ast ast, ContentBlock arg) {
		ContentBlock lastValue = arg;
		for (Ast child : ast.children())
			lastValue = visit(child, lastValue);
		return lastValue;
	}
	
	private void addPointersToEachOther(Block before, Block after) {
		before.afterBlocks.add(after);
		after.beforeBlocks.add(before);
	}
	
	@Override
	public ContentBlock assign(Assign ast, ContentBlock arg) {
		arg.body.add(ast);
		return arg;
	}

	@Override
	public ContentBlock builtInWrite(BuiltInWrite ast, ContentBlock arg) {
		arg.body.add(ast);
		return arg;
	}

	@Override
	public ContentBlock builtInWriteFloat(BuiltInWriteFloat ast, ContentBlock arg) {
		arg.body.add(ast);
		return arg;
	}

	@Override
	public ContentBlock builtInWriteln(BuiltInWriteln ast, ContentBlock arg) {
		arg.body.add(ast);
		return arg;
	}

	@Override
	public ContentBlock methodDecl(MethodDecl ast, ContentBlock arg) {
		ContentBlock bodyBlock = new ContentBlock();
		ContentBlock afterBodyBlock;
		
		addPointersToEachOther(start,bodyBlock);
		afterBodyBlock = this.visitChildren(ast.rwChildren.get(1), bodyBlock);
		
		return afterBodyBlock;
	}

	@Override
	public ContentBlock ifElse(IfElse ast, ContentBlock arg) {
		ContentBlock ifBlock;
		ContentBlock afterBlock = new ContentBlock();
		
		if(arg.hasContent()) {
			ifBlock = new  ContentBlock();
			addPointersToEachOther(arg,ifBlock);
		}
		else {
			ifBlock = arg;
		}
		
		ifBlock.body.add(ast);
		
		ContentBlock thenBlock = new  ContentBlock();
		addPointersToEachOther(ifBlock,thenBlock);
		ContentBlock thenReturnBlock = this.visit(ast.then(), thenBlock);
		addPointersToEachOther(thenReturnBlock,afterBlock);
		
		if(ast.otherwise() instanceof Nop) {
			addPointersToEachOther(ifBlock,afterBlock);
		}
		else {
			ContentBlock elseBlock = new  ContentBlock();
			addPointersToEachOther(ifBlock,elseBlock);
			ContentBlock elseReturnBlock = this.visit(ast.otherwise(), elseBlock);
			addPointersToEachOther(elseReturnBlock,afterBlock);
		}
		
		return afterBlock;
	}

	@Override
	public ContentBlock returnStmt(ReturnStmt ast, ContentBlock arg) {
		arg.body.add(ast);
		return arg;
	}

	@Override
	public ContentBlock whileLoop(WhileLoop ast, ContentBlock arg) {
		ContentBlock whileBlock;
		ContentBlock afterBlock = new ContentBlock();
		
		if(arg.hasContent()) {
			whileBlock = new  ContentBlock();
			addPointersToEachOther(arg,whileBlock);
		}
		else {
			whileBlock = arg;
		}
		
		whileBlock.body.add(ast);
		
		ContentBlock bodyBlock = new  ContentBlock();
		addPointersToEachOther(whileBlock,bodyBlock);
		ContentBlock bodyReturnBlock = this.visit(ast.body(), bodyBlock);
		
		addPointersToEachOther(bodyReturnBlock,whileBlock);
		addPointersToEachOther(bodyReturnBlock,afterBlock);
		
		return afterBlock;
	}

}
