package cd.optimize;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import cd.flow.Block;
import cd.flow.FlowAnalysis;
import cd.flow.FlowGraph;
import cd.flow.Block.ContentBlock;
import cd.ir.Ast;
import cd.ir.Ast.Assign;
import cd.ir.Ast.BinaryOp;
import cd.ir.Ast.BooleanConst;
import cd.ir.Ast.BuiltInRead;
import cd.ir.Ast.BuiltInReadFloat;
import cd.ir.Ast.BuiltInWrite;
import cd.ir.Ast.BuiltInWriteFloat;
import cd.ir.Ast.BuiltInWriteln;
import cd.ir.Ast.Cast;
import cd.ir.Ast.ClassDecl;
import cd.ir.Ast.Decl;
import cd.ir.Ast.Expr;
import cd.ir.Ast.Field;
import cd.ir.Ast.FloatConst;
import cd.ir.Ast.IfElse;
import cd.ir.Ast.Index;
import cd.ir.Ast.IntConst;
import cd.ir.Ast.MethodCall;
import cd.ir.Ast.MethodCallExpr;
import cd.ir.Ast.MethodDecl;
import cd.ir.Ast.NewArray;
import cd.ir.Ast.NewObject;
import cd.ir.Ast.Nop;
import cd.ir.Ast.NullConst;
import cd.ir.Ast.ReturnStmt;
import cd.ir.Ast.Seq;
import cd.ir.Ast.Stmt;
import cd.ir.Ast.ThisRef;
import cd.ir.Ast.UnaryOp;
import cd.ir.Ast.Var;
import cd.ir.Ast.VarDecl;
import cd.ir.Ast.WhileLoop;
import cd.ir.AstVisitor;
import cd.ir.ExprVisitor;
import cd.ir.Symbol.VariableSymbol;

public class DeadAssAnalyser extends FlowAnalysis<Block, Set<String>> {
	
	private HashMap<Block,Set<String>> blockToUseSet = new HashMap<Block,Set<String>>();
	
	private HashMap<Block,Set<String>> blockToDefSet = new HashMap<Block,Set<String>>();
	
	public DeadAssAnalyser(FlowGraph<Block> fg) {
		super(fg,false);
		 calcUseAndDef();
	}
	
	private void calcUseAndDef() {
		UseVisitor uv= new UseVisitor();
		
		for(Block b : graph.getForwardOrder()) {
			HashSet<String> varsUsedBefore = new HashSet<String>();
			HashSet<String> varsDefinedBefore = new HashSet<String>();
			
			for(Stmt s : b.body) {
				if(s instanceof Assign) {
					Assign a = (Assign) s;
					if(a.left() instanceof Var && !a.left().type.isArrayType()) {
						Set<String> useSet = uv.getUse(a.right());
						useSet.removeAll(varsDefinedBefore);
						varsUsedBefore.addAll(useSet);
						Var var = ((Var) a.left());
						if(!varsUsedBefore.contains(var.name)) {
							if(!var.sym.kind.equals(VariableSymbol.Kind.FIELD))
								varsDefinedBefore.add(var.name);
						}
					} 
					else {
						Set<String> useSet = uv.getUse(a.right());
						useSet.removeAll(varsDefinedBefore);
						varsUsedBefore.addAll(useSet);
						useSet = uv.getUse(a.left());
						useSet.removeAll(varsDefinedBefore);
						varsUsedBefore.addAll(useSet);
					}
				}
				else {
					Set<String> useSet = uv.getUse(s);
					useSet.removeAll(varsDefinedBefore);
					varsUsedBefore.addAll(useSet);
				}
			}
			
			blockToUseSet.put(b, varsUsedBefore);
			blockToDefSet.put(b, varsDefinedBefore);
		}
	}
	
	
	
	public Set<String> getUseOfBlock(Block b) {
		return blockToUseSet.get(b);
	}
	
	public Set<String> getDefOfBlock(Block b) {
		return blockToDefSet.get(b);
		
	}

	@Override
	protected void flowThrough(Set<String> in, Block node, Set<String> out) {
		Set<String> use = getUseOfBlock(node);
		Set<String> def = getDefOfBlock(node);
		HashSet<String> temp = new HashSet<String>();
		out.clear();
		temp.addAll(in);
		temp.removeAll(def);
		out.addAll(temp);
		out.addAll(use);
	}

	@Override
	protected void mergeInto(Set<String> inSet, Set<String> out, Block infFront) {
		out.addAll(inSet);
	}

	@Override
	protected Set<String> getEntryInitFlow() {
		return new HashSet<String>();
	}

	@Override
	protected Set<String> getInitFlow() {
		return new HashSet<String>();
	}

	@Override
	protected void copy(Set<String> src, Set<String> dest) {
		dest.clear();
		dest.addAll(src);
	}

	@Override
	protected boolean isEqual(Set<String> a1, Set<String> a2) {
		return a1.containsAll(a2);
	}
	

}
