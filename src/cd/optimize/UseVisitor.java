package cd.optimize;

import java.util.HashSet;
import java.util.Set;

import cd.ir.Ast;
import cd.ir.AstVisitor;
import cd.ir.Ast.BinaryOp;
import cd.ir.Ast.BooleanConst;
import cd.ir.Ast.BuiltInRead;
import cd.ir.Ast.BuiltInReadFloat;
import cd.ir.Ast.BuiltInWrite;
import cd.ir.Ast.BuiltInWriteFloat;
import cd.ir.Ast.BuiltInWriteln;
import cd.ir.Ast.Cast;
import cd.ir.Ast.Expr;
import cd.ir.Ast.Field;
import cd.ir.Ast.FloatConst;
import cd.ir.Ast.IfElse;
import cd.ir.Ast.Index;
import cd.ir.Ast.IntConst;
import cd.ir.Ast.MethodCall;
import cd.ir.Ast.MethodCallExpr;
import cd.ir.Ast.NewArray;
import cd.ir.Ast.NewObject;
import cd.ir.Ast.NullConst;
import cd.ir.Ast.ReturnStmt;
import cd.ir.Ast.ThisRef;
import cd.ir.Ast.UnaryOp;
import cd.ir.Ast.Var;
import cd.ir.Ast.WhileLoop;
import cd.ir.Symbol.VariableSymbol;

class UseVisitor extends AstVisitor<Set<String>, Set<String>> {
	
	
	@Override
	public Set<String> builtInWrite(BuiltInWrite ast, Set<String> arg) {
		return this.visit(ast.arg(), arg);
	}

	@Override
	public Set<String> builtInWriteFloat(BuiltInWriteFloat ast, Set<String> arg) {
		return this.visit(ast.arg(), arg);
	}

	@Override
	public Set<String> builtInWriteln(BuiltInWriteln ast, Set<String> arg) {
		return arg;
	}

	@Override
	public Set<String> ifElse(IfElse ast, Set<String> arg) {
		return this.visit(ast.condition(),arg);
	}

	@Override
	public Set<String> returnStmt(ReturnStmt ast, Set<String> arg) {
		if(ast.arg() == null)
			return arg;
		return this.visit(ast.arg(),arg);
	}

	@Override
	public Set<String> methodCall(MethodCall ast, Set<String> arg) {
		for(Expr exp : ast.allArguments()) {
			this.visit(exp, arg);
		}
		return arg;
	}

	@Override
	public Set<String> whileLoop(WhileLoop ast, Set<String> arg) {
		return this.visit(ast.condition(), arg);
	}

	@Override
	public Set<String> binaryOp(BinaryOp ast, Set<String> arg) {
		return this.visitChildren(ast, arg);
	}

	@Override
	public Set<String> booleanConst(BooleanConst ast, Set<String> arg) {
		return arg;
	}

	@Override
	public Set<String> builtInRead(BuiltInRead ast, Set<String> arg) {
		return arg;
	}

	@Override
	public Set<String> builtInReadFloat(BuiltInReadFloat ast, Set<String> arg) {
		return arg;
	}

	@Override
	public Set<String> cast(Cast ast, Set<String> arg) {
		return this.visit(ast.arg(), arg);
	}

	@Override
	public Set<String> field(Field ast, Set<String> arg) {
		return this.visit(ast.arg(), arg);
	}

	@Override
	public Set<String> index(Index ast, Set<String> arg) {
		return this.visitChildren(ast, arg);
	}

	@Override
	public Set<String> intConst(IntConst ast, Set<String> arg) {
		return arg;
	}

	@Override
	public Set<String> floatConst(FloatConst ast, Set<String> arg) {
		return arg;
	}

	@Override
	public Set<String> methodCall(MethodCallExpr ast, Set<String> arg) {
		for(Expr exp : ast.allArguments()) {
			this.visit(exp, arg);
		}
		return arg;
	}

	@Override
	public Set<String> newObject(NewObject ast, Set<String> arg) {
		return arg;
	}

	@Override
	public Set<String> newArray(NewArray ast, Set<String> arg) {
		return this.visit(ast.arg(), arg);
	}

	@Override
	public Set<String> nullConst(NullConst ast, Set<String> arg) {
		return arg;
	}

	@Override
	public Set<String> thisRef(ThisRef ast, Set<String> arg) {
		return arg;
	}

	@Override
	public Set<String> unaryOp(UnaryOp ast, Set<String> arg) {
		return this.visit(ast.arg(), arg);
	}

	public Set<String> getUse(Ast e) {
		Set<String> res = this.visit(e, new HashSet<String>());
		if(res == null)
			return new HashSet<String>();
		return res;
	}

	@Override
	public Set<String> var(Var ast, Set<String> arg) {
		if(!ast.type.isArrayType() && !ast.sym.kind.equals(VariableSymbol.Kind.FIELD))
			arg.add(ast.name);
		return arg;
	}
}
