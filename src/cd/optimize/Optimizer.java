package cd.optimize;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cd.flow.Block;
import cd.flow.BlockFlowGraph;
import cd.ir.Ast.Assign;
import cd.ir.Ast.BuiltInRead;
import cd.ir.Ast.BuiltInReadFloat;
import cd.ir.Ast.BuiltInWrite;
import cd.ir.Ast.BuiltInWriteFloat;
import cd.ir.Ast.ClassDecl;
import cd.ir.Ast.Expr;
import cd.ir.Ast.IfElse;
import cd.ir.Ast.MethodCall;
import cd.ir.Ast.MethodCallExpr;
import cd.ir.Ast.MethodDecl;
import cd.ir.Ast.ReturnStmt;
import cd.ir.Ast.Seq;
import cd.ir.Ast.Stmt;
import cd.ir.Ast.Var;
import cd.ir.Ast.WhileLoop;
import cd.ir.Ast;
import cd.ir.AstVisitor;
import cd.ir.ExprVisitor;
import cd.ir.Symbol.VariableSymbol;

public class Optimizer {
	
	
	public Optimizer() {
	}
	
	private Map<MethodDecl,BlockFlowGraph> getFlowGraphs(List<ClassDecl> astRoots) {
		Map<MethodDecl,BlockFlowGraph> methodToGraph = new HashMap<MethodDecl,BlockFlowGraph>();
		BlockGenerator blockGenerator = new BlockGenerator();
		for(ClassDecl cd : astRoots) {
			for(MethodDecl md : cd.methods()) {
				BlockFlowGraph bfg = blockGenerator.getBlockFlowGraph(md);
				methodToGraph.put(md, bfg);
			}
		}
		return methodToGraph;
	}

	public void doConstantFolding(List<ClassDecl> astRoots) {
		Map<MethodDecl,BlockFlowGraph> methodToGraph = getFlowGraphs(astRoots);
		for(MethodDecl md : methodToGraph.keySet()) {
			BlockFlowGraph graph = methodToGraph.get(md);
			ConstantFlowAnalysis cfa = new ConstantFlowAnalysis(graph,md.sym.locals,md.sym.parameters);
			cfa.doAnalysis();
			ConstFoldVisitor cfv = new ConstFoldVisitor();
			
			for(Block b : graph.getForwardOrder()) {
				HashMap<String,Expr> workingSet = new HashMap<String,Expr>();
				//get the in set to our working set
				cfa.copy(cfa.getINSet(b), workingSet);
				for(Stmt s:b.body){
					if(s instanceof Assign){
						Assign assign;
						Var var;
						//we are only interested in new definitions here
						assign = (Assign)s;
						
						//we visit both expressions anyway because the visitor might optimize a subtree
						Expr expr = cfv.visit(assign.right(), workingSet);
						cfv.visit(assign.left(), workingSet);
						
						if(expr != null){
							//we have a constant expresssion
							assign.setRight(expr);
						}
						if(assign.left() instanceof Var){
							//we are only interested in variables here
							var = (Var)assign.left();
							if((var.sym.kind == VariableSymbol.Kind.LOCAL || var.sym.kind == VariableSymbol.Kind.PARAM)){
								//we only care if the variable is a local or a parameter and is not of reference type
								
								//add/change the definition of this variable in our working set
								workingSet.put(var.name, expr);
								
							}
						}
					}
					else if(s instanceof WhileLoop){
						WhileLoop loop = (WhileLoop) s;
						Expr expr = cfv.visit(loop.condition(), workingSet);
						if(expr != null){
							//we have a constant condition
							loop.setCondition(expr);
						}
					}
					else if(s instanceof MethodCall){
						//TODO
						MethodCall meth = (MethodCall) s;
						
						ArrayList<Expr> arglist = new ArrayList<Expr>();
						Iterator<Expr> it = meth.allArguments().iterator();
						Expr argument, expr;
						while(it.hasNext()){
							argument = it.next();
							expr = cfv.visit(argument, workingSet);
							if(expr != null){
								//this argument is a constant and we add it to the list
								arglist.add(expr);
							}
							else{
								//keep the other argument
								arglist.add(argument);
							}
						}
						it = arglist.iterator();
						int i=0;
						while(it.hasNext()){
							meth.setArgument(it.next(), i);
							i=i+1;
						}
					}
					else if(s instanceof IfElse){
						IfElse ifelse = (IfElse) s;
						Expr expr = cfv.visit(ifelse.condition(), workingSet);
						if(expr != null){
							ifelse.setCondition(expr);
						}
					}
					else if(s instanceof BuiltInWrite){
						BuiltInWrite biw = (BuiltInWrite) s;
						Expr expr = cfv.visit(biw.arg(), workingSet);
						if(expr != null){
							biw.setArg(expr);
						}
					}
					else if(s instanceof BuiltInWriteFloat){
						BuiltInWriteFloat biwf = (BuiltInWriteFloat)s;
						Expr expr = cfv.visit(biwf.arg(), workingSet);
						if(expr != null){
							biwf.setArg(expr);
						}
					}
					else if(s instanceof ReturnStmt){
						ReturnStmt rs = (ReturnStmt)s;
						Expr expr = null;
						if(rs.arg() != null){
							//we only optimize it if it's not an empty return statement
							expr = cfv.visit(rs.arg(), workingSet);
						}
						if(expr != null){
							rs.setArg(expr);
						}
					}
				}
				
			}
		}
	}
	
	public void doDeadAssignmentElem(List<ClassDecl> astRoots) {
		boolean doAgain;
		do { 
			doAgain = false;
			Map<MethodDecl,BlockFlowGraph> methodToGraph = getFlowGraphs(astRoots);
			for(final MethodDecl md : methodToGraph.keySet()) {
				BlockFlowGraph graph = methodToGraph.get(md);
				DeadAssAnalyser daa = new DeadAssAnalyser(graph);
				daa.doAnalysis();
				
				for(Block b : graph.getForwardOrder()) {
					Set<String> out = daa.getOUTSet(b);
					RemoveLogic logic = new RemoveLogic(out, md);
					for(int i=b.body.size()-1;i>=0;i--) {
						Stmt s= b.body.get(i);
						logic.iter(s);
					}
					if(logic.hasRemoved)
						doAgain=true;
				}
			}
		} while(doAgain);
	}
	
	private class RemoveLogic extends AstVisitor<String,Void> {
		
		private HashSet<String> curLive; 
		private UseVisitor uv = new UseVisitor();
		private MethodDecl md;
		public boolean hasRemoved = false;
		
		public RemoveLogic(Set<String> out, MethodDecl md) {
			curLive = new HashSet<String>();
			curLive.addAll(out);
			this.md = md;
		}
		
		public void iter(Stmt s) {
			this.visit(s, null);
		}
		
		@Override
		protected String dfltStmt(Stmt ast, Void arg) {
			Set<String> live= uv.getUse(ast);
			curLive.addAll(live);
			return null;
		}
		
		@Override
		public String var(Ast.Var ast, Void arg) {
			if(ast.type.isArrayType())
				return null;
			if(ast.sym.kind.equals(VariableSymbol.Kind.FIELD))
				return null;
			return ast.name;
		}

		@Override
		public String assign(Assign ast, Void arg) {
			Set<String> live;
			String left = null;
			if(ast.left() instanceof Var) {
				 left = this.visit(ast.left(),null);
			}
			if(left==null) {
				live= uv.getUse(ast.right());
				curLive.addAll(live);
				live= uv.getUse(ast.left());
				curLive.addAll(live);
			}
			else {
				if(curLive.contains(left)) {
					curLive.remove(left);
					live = uv.getUse(ast.right());
					curLive.addAll(live);
				}
				else {
					CheckMethodCalls hmcv = new CheckMethodCalls();
					if(!hmcv.hasMethodcall(ast.right())) {
						hasRemoved  = true;
						final Assign toRemove = ast;
						new AstVisitor<Void,Ast>() {
							@Override
							protected Void dfltStmt(Stmt ast, Ast arg) {
								return null;
							}

							@Override
							public Void assign(Assign ast, Ast arg) {
								if(ast.equals(toRemove))
									arg.rwChildren.remove(toRemove);
								return null;
							}

							@Override
							public Void seq(Seq ast, Ast arg) {
								this.visitChildren(ast, ast);
								return null;
							}
							
							@Override
							public Void ifElse(IfElse ast, Ast arg) {
								this.visitChildren(ast, ast);
								return null;
							}

							@Override
							public Void whileLoop(WhileLoop ast, Ast arg) {
								this.visitChildren(ast, ast);
								return null;
							}
						}.visit(md.body(), md.body());
					}
					else {
						curLive.remove(left);
						live = uv.getUse(ast.right());
						curLive.addAll(live);
					}
						
				}
			}
			
			return null;
		}
	}
	
	private class CheckMethodCalls extends ExprVisitor<Boolean,Boolean> {
		
		@Override
		public Boolean builtInRead(BuiltInRead ast, Boolean arg) {
			return true;
		}

		@Override
		public Boolean builtInReadFloat(BuiltInReadFloat ast, Boolean arg) {
			return true;
		}

		public boolean hasMethodcall(Expr e) {
			return this.visit(e, false);
		}
		
		@Override
		protected Boolean dfltExpr(Expr ast, Boolean arg) {
			if(arg)
				return arg;
			for(Ast e : ast.rwChildren) {
				if(this.visit((Expr) e, arg))
					return true;
			}
			return false;
		}

		@Override
		public Boolean methodCall(MethodCallExpr ast, Boolean arg) {
			return true;
		}
		
	}
	
}
