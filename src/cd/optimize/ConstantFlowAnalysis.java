package cd.optimize;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cd.flow.Block;
import cd.flow.FlowAnalysis;
import cd.flow.FlowGraph;
import cd.ir.Ast.Assign;
import cd.ir.Ast.BooleanConst;
import cd.ir.Ast.Expr;
import cd.ir.Ast.FloatConst;
import cd.ir.Ast.IntConst;
import cd.ir.Ast.NullConst;
import cd.ir.Ast.Seq;
import cd.ir.Ast.Stmt;
import cd.ir.Ast.Var;
import cd.ir.Symbol.VariableSymbol;

public class ConstantFlowAnalysis extends FlowAnalysis<Block,HashMap<String, Expr>>{

	/* if the HashMap does not contain a key, the value of the variable is bottom
	 * if it contains a key but the value is null, it's top
	 * if it contains a expression, the variable has a constant value
	 */
	
	private Map<String, VariableSymbol> locals;
	private List<VariableSymbol> parameters;
	ConstFoldCheckVisitor cfc;
	
	public ConstantFlowAnalysis(FlowGraph fg, Map<String, VariableSymbol> local, List<VariableSymbol> parameter) {
		super(fg,true);
		this.locals = local;
		this.parameters = parameter;
		this.cfc = new ConstFoldCheckVisitor();
	}

	
	@Override
	protected void flowThrough(HashMap<String, Expr> in, Block node,
			HashMap<String, Expr> out) {
		
		Iterator<Stmt> it = node.body.listIterator();
		Assign assign;
		Stmt stmt;
		Var var;
		HashMap<String,Expr> workingSet = new HashMap<String,Expr>();
		this.copy(in, workingSet);
		while(it.hasNext()){
			stmt = it.next();
			if(stmt instanceof Assign){
				//we are only interested in new definitions here
				assign = (Assign)stmt;
				if(assign.left() instanceof Var){
					//we are only interested in variables here
					var = (Var)assign.left();
					if((var.sym.kind == VariableSymbol.Kind.LOCAL || var.sym.kind == VariableSymbol.Kind.PARAM)){
						//we only care if the variable is a local or a parameter
						
						//add/change the definition of this variable in our working set
						workingSet.put(var.name, cfc.visit(assign.right(), workingSet));
					}
				}
			}
		}
		//copy our working set to the out set
		this.copy(workingSet, out);
	}

	@Override
	protected void mergeInto(HashMap<String, Expr> inSet,
			HashMap<String, Expr> out, Block infFront) {
		IntConst intConst, intConst2;
		FloatConst floatConst, floatConst2;
		BooleanConst booleanConst, booleanConst2;
		NullConst nullConst;
		Expr expr;
		Set<String> inKeySet = inSet.keySet();
		Iterator<String> it = inKeySet.iterator();
		String var;
		while(it.hasNext()){
			var=it.next();
			expr = inSet.get(var);
			if(expr != null){
				if(expr instanceof IntConst){
					
					intConst = (IntConst) expr;
					if(out.containsKey(var)){
						//var is not bottom in out set
						if(out.get(var) == null){
							//var is top in out set, keep it top
						}
						else{
							//var has a constant value in out set -> compare them
							intConst2 = (IntConst) out.get(var);
							if(intConst.value == intConst2.value){
								//they have the same value, no need to change anything
							}
							else{
								//they have different values, change it to top
								out.put(var, null);
							}
						}
					}
					else{
						//var is bottom in out set, simply insert our value
						out.put(var, intConst);
					}
				}
				else if(expr instanceof FloatConst){
					floatConst = (FloatConst) expr;
					if(out.containsKey(var)){
						//var is not bottom in out set
						if(out.get(var) == null){
							//var is top in out set, keep it top
						}
						else{
							//var has a constant value in out set -> compare them
							floatConst2 = (FloatConst) out.get(var);
							if(floatConst.value == floatConst2.value){
								//they have the same value, no need to change anything
							}
							else{
								//they have different values, change it to top
								out.put(var, null);
							}
						}
					}
					else{
						//var is bottom in out set, simply insert our value
						out.put(var, floatConst);
					}
				}
				else if(expr instanceof BooleanConst){
					booleanConst = (BooleanConst) expr;
					if(out.containsKey(var)){
						//var is not bottom in out set
						if(out.get(var) == null){
							//var is top in out set, keep it top
						}
						else{
							//var has a constant value in out set -> compare them
							booleanConst2 = (BooleanConst) out.get(var);
							if(booleanConst.value == booleanConst2.value){
								//they have the same value, no need to change anything
							}
							else{
								//they have different values, change it to top
								out.put(var, null);
							}
						}
					}
					else{
						//var is bottom in out set, simply insert our value
						out.put(var, booleanConst);
					}
				}
				else if(expr instanceof NullConst){
					nullConst = (NullConst) expr;
					if(out.containsKey(var)){
						if(out.get(var) == null){
							//var is top in out set, keep it top
						}
						else{
							//var is a null const in both sets, we keep it that way
						}
					}
					else{
						//var is bottom in out set, simply insert our value
						out.put(var, nullConst);
					}
					
				}
			}
			else{
				//we insert null into out because the variable has value top
				out.put(var, null);
			}
			
		}
		
		
	}

	@Override
	protected void copy(HashMap<String, Expr> src, HashMap<String, Expr> dest) {
		
		Set<String> destSet = dest.keySet();
		Iterator<String> it = destSet.iterator();
		String var;
		//remove all content from dest
		while(it.hasNext()){
			var=it.next();
			dest.remove(var);
			destSet = dest.keySet();
			it = destSet.iterator();
		}
		//add all content from src to dest
		Set<String> srcSet = src.keySet();
		it = srcSet.iterator();
		while(it.hasNext()){
			var=it.next();
			dest.put(var, src.get(var));
		}
	}


	@Override
	protected HashMap<String, Expr> getEntryInitFlow() {
		HashMap<String,Expr> sol = new HashMap<String, Expr>();
		
		//add the locals
		Set<String> varSet = locals.keySet();
		Iterator<String> it = varSet.iterator();
		String var;
		VariableSymbol varSym;
		while(it.hasNext()){
			var = it.next();
			varSym = locals.get(var);
			//we add it to our set with value top
			sol.put(var, null);
		}
		
		//add the parameters
		Iterator<VariableSymbol> it2 = parameters.iterator();
		VariableSymbol var2;
		while(it2.hasNext()){
			var2=it2.next();
			//we add it to our set with value top
			sol.put(var2.name, null);
		}
		return sol;
	}


	@Override
	protected HashMap<String, Expr> getInitFlow() {
		//everything is bottom
		return new HashMap<String,Expr>();
	}


	@Override
	protected boolean isEqual(HashMap<String, Expr> a1, HashMap<String, Expr> a2) {
		Set<String> set;
		
		//check whether all the keys from a1 are in a2
		set=a1.keySet();
		Iterator<String> it = set.iterator();
		String var;
		while(it.hasNext()){
			var=it.next();
			if(!a2.containsKey(var)){
				return false;
			}
		}
		//check whether all the keys from a2 are in a1
		set=a2.keySet();
		it = set.iterator();
		while(it.hasNext()){
			var=it.next();
			if(!a1.containsKey(var)){
				return false;
			}
		}
		
		//if we get here, we know that both key sets are equal
		Expr expr1, expr2;
		it = set.iterator();
		while(it.hasNext()){
			var = it.next();
			expr1 = a1.get(var);
			expr2 = a2.get(var);
			if(expr1 == null){
				if(expr2 == null){
					//do nothing
				}
				else{
					return false;
				}
			}
			else{
				if(expr2 == null){
					return false;
				}
				else{
					if(expr1 instanceof IntConst){
						if(((IntConst)expr1).value != ((IntConst)expr2).value){
							return false;
						}
					}
					else if(expr1 instanceof FloatConst){
						if(((FloatConst)expr1).value != ((FloatConst)expr2).value){
							return false;
						}
					}
					else if(expr1 instanceof BooleanConst){
						if(((BooleanConst)expr1).value != ((BooleanConst)expr2).value){
							return false;
						}
					}
					else{
						//the expressions are of type NullConst and are therefore equal
					}
				}
			}
		}
		
		return true;
	}

}
