package cd.optimize;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import cd.ir.Ast.BinaryOp;
import cd.ir.Ast.BooleanConst;
import cd.ir.Ast.BuiltInRead;
import cd.ir.Ast.BuiltInReadFloat;
import cd.ir.Ast.Cast;
import cd.ir.Ast.Expr;
import cd.ir.Ast.Field;
import cd.ir.Ast.FloatConst;
import cd.ir.Ast.Index;
import cd.ir.Ast.IntConst;
import cd.ir.Ast.MethodCallExpr;
import cd.ir.Ast.NewArray;
import cd.ir.Ast.NewObject;
import cd.ir.Ast.NullConst;
import cd.ir.Ast.ThisRef;
import cd.ir.Ast.UnaryOp;
import cd.ir.Ast.Var;
import cd.ir.ExprVisitor;

public class ConstFoldVisitor extends ExprVisitor<Expr, HashMap<String, Expr>>{


	@Override
	public Expr binaryOp(BinaryOp ast, HashMap<String, Expr> arg) {
		BooleanConst bool1, bool2;
		IntConst int1, int2;
		FloatConst float1, float2;
		
		Expr expr1 = this.visit(ast.left(), arg);
		Expr expr2 = this.visit(ast.right(), arg);
		
		if(expr1 != null){
			ast.setLeft(expr1);
		}
		if(expr2 != null){
			ast.setRight(expr2);
		}
		
		Expr sol=null;
		
		if(expr1!=null && expr2!=null){
			switch (ast.operator) {
			case B_PLUS:
				if(expr1 instanceof IntConst){
					int1 = (IntConst) expr1;
					int2 = (IntConst) expr2;
					sol = new IntConst(int1.value + int2.value);
				}
				else{
					float1 = (FloatConst) expr1;
					float2 = (FloatConst) expr2;
					sol = new FloatConst(float1.value + float2.value);
				}
				break;
			case B_MINUS:
				if(expr1 instanceof IntConst){
					int1 = (IntConst) expr1;
					int2 = (IntConst) expr2;
					sol = new IntConst(int1.value - int2.value);
				}
				else{
					float1 = (FloatConst) expr1;
					float2 = (FloatConst) expr2;
					sol = new FloatConst(float1.value - float2.value);
				}
				break;
			case B_TIMES:
				if(expr1 instanceof IntConst){
					int1 = (IntConst) expr1;
					int2 = (IntConst) expr2;
					sol = new IntConst(int1.value * int2.value);
				}
				else{
					float1 = (FloatConst) expr1;
					float2 = (FloatConst) expr2;
					sol = new FloatConst(float1.value * float2.value);
				}
				break;
			case B_DIV:
				if(expr1 instanceof IntConst){
					int1 = (IntConst) expr1;
					int2 = (IntConst) expr2;
					sol = new IntConst(int1.value / int2.value);
				}
				else{
					float1 = (FloatConst) expr1;
					float2 = (FloatConst) expr2;
					sol = new FloatConst(float1.value / float2.value);
				}
				break;
			case B_MOD:
				int1 = (IntConst) expr1;
				int2 = (IntConst) expr2;
				sol = new IntConst(int1.value % int2.value);
				break;
			case B_AND:
				bool1 = (BooleanConst) expr1;
				bool2 = (BooleanConst) expr2;
				sol = new BooleanConst(bool1.value && bool2.value);
				break;
			case B_OR:
				bool1 = (BooleanConst) expr1;
				bool2 = (BooleanConst) expr2;
				sol = new BooleanConst(bool1.value || bool2.value);
				break;
			case B_EQUAL:
				if(expr1 instanceof IntConst){
					int1 = (IntConst) expr1;
					int2 = (IntConst) expr2;
					sol = new BooleanConst(int1.value == int2.value);
				}
				else if(expr1 instanceof FloatConst){
					float1 = (FloatConst) expr1;
					float2 = (FloatConst) expr2;
					sol = new BooleanConst(float1.value == float2.value);
				}
				else if(expr1 instanceof BooleanConst){
					bool1 = (BooleanConst) expr1;
					bool2 = (BooleanConst) expr2;
					sol = new BooleanConst(bool1.value == bool2.value);
				}
				else if(expr1 instanceof NullConst){
					//return true because both are null const
					sol = new BooleanConst(true);
				}
				break;
			case B_NOT_EQUAL:
				if(expr1 instanceof IntConst){
					int1 = (IntConst) expr1;
					int2 = (IntConst) expr2;
					sol = new BooleanConst(int1.value != int2.value);
				}
				else if(expr1 instanceof FloatConst){
					float1 = (FloatConst) expr1;
					float2 = (FloatConst) expr2;
					sol = new BooleanConst(float1.value != float2.value);
				}
				else if(expr1 instanceof BooleanConst){
					bool1 = (BooleanConst) expr1;
					bool2 = (BooleanConst) expr2;
					sol = new BooleanConst(bool1.value != bool2.value);
				}
				else if(expr1 instanceof NullConst){
					//return false because both are null const
					sol = new BooleanConst(false);
				}
				break;
			case B_LESS_THAN:
				if(expr1 instanceof IntConst){
					int1 = (IntConst) expr1;
					int2 = (IntConst) expr2;
					sol = new BooleanConst(int1.value < int2.value);
				}
				else if(expr1 instanceof FloatConst){
					float1 = (FloatConst) expr1;
					float2 = (FloatConst) expr2;
					sol = new BooleanConst(float1.value < float2.value);
				}
				break;
			case B_LESS_OR_EQUAL:
				if(expr1 instanceof IntConst){
					int1 = (IntConst) expr1;
					int2 = (IntConst) expr2;
					sol = new BooleanConst(int1.value <= int2.value);
				}
				else if(expr1 instanceof FloatConst){
					float1 = (FloatConst) expr1;
					float2 = (FloatConst) expr2;
					sol = new BooleanConst(float1.value <= float2.value);
				}
				break;
			case B_GREATER_THAN:
				if(expr1 instanceof IntConst){
					int1 = (IntConst) expr1;
					int2 = (IntConst) expr2;
					sol = new BooleanConst(int1.value > int2.value);
				}
				else if(expr1 instanceof FloatConst){
					float1 = (FloatConst) expr1;
					float2 = (FloatConst) expr2;
					sol = new BooleanConst(float1.value > float2.value);
				}
				break;
			case B_GREATER_OR_EQUAL:
				if(expr1 instanceof IntConst){
					int1 = (IntConst) expr1;
					int2 = (IntConst) expr2;
					sol = new BooleanConst(int1.value >= int2.value);
				}
				else if(expr1 instanceof FloatConst){
					float1 = (FloatConst) expr1;
					float2 = (FloatConst) expr2;
					sol = new BooleanConst(float1.value >= float2.value);
				}
				break;
	        default:
	        	throw new RuntimeException("Not required");
			}
		}
		
		return sol;
	}

	@Override
	public Expr booleanConst(BooleanConst ast, HashMap<String, Expr> arg) {
		return ast;
	}

	@Override
	public Expr builtInRead(BuiltInRead ast, HashMap<String, Expr> arg) {
		return null;
	}

	@Override
	public Expr builtInReadFloat(BuiltInReadFloat ast, HashMap<String, Expr> arg) {
		return null;
	}

	@Override
	public Expr cast(Cast ast, HashMap<String, Expr> arg) {
		//we can't cast primitive types, hence we are not interested here
		
		return this.visit(ast.arg(), arg);
	}

	@Override
	public Expr field(Field ast, HashMap<String, Expr> arg) {
		this.visit(ast.arg(), arg);
		return null;
	}

	@Override
	public Expr index(Index ast, HashMap<String, Expr> arg) {
		
		Expr expr = this.visit(ast.right(), arg);
		if(expr != null){
			//expr is a constant
			ast.setRight(expr);
		}
		this.visit(ast.left(), arg);
		return null;
	}

	@Override
	public Expr intConst(IntConst ast, HashMap<String, Expr> arg) {
		return ast;
	}

	@Override
	public Expr floatConst(FloatConst ast, HashMap<String, Expr> arg) {
		return ast;
	}

	@Override
	public Expr methodCall(MethodCallExpr ast, HashMap<String, Expr> arg) {
		ArrayList<Expr> arglist = new ArrayList<Expr>();
		Iterator<Expr> it = ast.allArguments().iterator();
		Expr argument, expr;
		while(it.hasNext()){
			argument = it.next();
			expr = this.visit(argument, arg);
			if(expr != null){
				//this argument is a constant and we add it to the list
				arglist.add(expr);
			}
			else{
				//keep the other argument
				arglist.add(argument);
			}
		}
		it = arglist.iterator();
		int i=0;
		while(it.hasNext()){
			ast.setArgument(it.next(), i);
			i=i+1;
		}
		return null;
	}

	@Override
	public Expr newObject(NewObject ast, HashMap<String, Expr> arg) {
		return null;
	}

	@Override
	public Expr newArray(NewArray ast, HashMap<String, Expr> arg) {
		Expr expr = this.visit(ast.arg(), arg);
		if(expr != null){
			//expr is a constant
			ast.setArg(expr);
		}
		return null;
	}

	@Override
	public Expr nullConst(NullConst ast, HashMap<String, Expr> arg) {
		return ast;
	}

	@Override
	public Expr thisRef(ThisRef ast, HashMap<String, Expr> arg) {
		return null;
	}

	@Override
	public Expr unaryOp(UnaryOp ast, HashMap<String, Expr> arg) {
		IntConst intConst;
		FloatConst floatConst;
		BooleanConst booleanConst;
		Expr expr = this.visit(ast.arg(), arg);
		if(expr != null){
			ast.setArg(expr);
		}
		
		if(expr != null){
			//expr is a constant
			switch (ast.operator) {
			case U_PLUS:
				//do nothing
				break;
			case U_MINUS:
				if(expr instanceof IntConst){
					intConst = (IntConst) expr;
					expr = new IntConst(- intConst.value);
				}
				else{
					floatConst = (FloatConst) expr;
					expr = new FloatConst( - floatConst.value);
				}
				break;
			case U_BOOL_NOT:
				booleanConst = (BooleanConst) expr;
				expr = new BooleanConst(!booleanConst.value);
				break;
			default:
				throw new RuntimeException("Not required");
			} 
		}
		return expr;
	}

	@Override
	public Expr var(Var ast, HashMap<String, Expr> arg) {
		return arg.get(ast.name);
	}

}
