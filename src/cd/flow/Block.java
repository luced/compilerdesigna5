package cd.flow;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import cd.ir.Ast.Stmt;

public abstract class Block {
	public List<Block> beforeBlocks;
	public List<Stmt> body;
	public List<Block> afterBlocks;
	
	public boolean hasContent() {
		return !(body==null || body.isEmpty());
	}
	
	public static class ContentBlock extends Block {
		
		
		public ContentBlock() {
			this.body = new ArrayList<Stmt>();
			this.beforeBlocks = new ArrayList<Block>();
			this.afterBlocks = new ArrayList<Block>();
		}
		
		public void addConentToEnd(Stmt conent) {
			this.body.add(conent);
		}
	}

	public static class StartBlock extends Block {
		
		public StartBlock() {
			this.beforeBlocks = new ArrayList<Block>();
			this.afterBlocks = new ArrayList<Block>();
			this.body = new ArrayList<Stmt>();
		}
	}
	
	public static class ExitBlock extends Block {
		
		public ExitBlock() {
			this.beforeBlocks = new ArrayList<Block>();
			this.afterBlocks = new ArrayList<Block>();
			this.body = new ArrayList<Stmt>();
		}
	}
}
