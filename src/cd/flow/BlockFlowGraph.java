package cd.flow;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cd.flow.Block.ExitBlock;
import cd.flow.Block.StartBlock;

public class BlockFlowGraph implements FlowGraph<Block> {
	
	private ArrayList<Block> nodes = new ArrayList<Block>();
	
	private StartBlock start;
	
	private ExitBlock end;

	public BlockFlowGraph(StartBlock start, ExitBlock end) {
		this.start = start;
		this.end  = end;
		initNodes(start);
	}
	
	protected void initNodes(StartBlock start) {
		ArrayDeque<Block> todo = new ArrayDeque<Block>();
		nodes.add(start);
		todo.add(start);
		while(!todo.isEmpty()) {
			Block cur = todo.pollFirst();
			for(Block b : cur.afterBlocks) {
				if(!nodes.contains(b)) {
					nodes.add(b);
					todo.add(b);
				}
			}
		}
	}
	
	@Override
	public Block getHead() {
		return start;
	}

	@Override
	public Block getTail() {
		return end;
	}

	@Override
	public List<Block> getPredsOf(Block s) {
		return s.beforeBlocks;
	}

	@Override
	public List<Block> getSuccsOf(Block s) {
		return s.afterBlocks;
	}

	@Override
	public int size() {
		return nodes.size();
	}

	@Override
	public Iterator<Block> iterator() {
		return nodes.iterator();
	}

	@Override
	public List<Block> getForwardOrder() {
		ArrayList<Block> fwBlocks = new ArrayList<Block>();
		for(Block b : nodes) {
			fwBlocks.add(b);
		}
		return fwBlocks;
	}

	@Override
	public List<Block> getBackwardOrder() {
		ArrayList<Block> bwBlocks = new ArrayList<Block>();
		for(int i=nodes.size()-1;i>=0;i--) {
			bwBlocks.add(nodes.get(i));
		}
		return bwBlocks;
	}
	
	
}
