package cd.flow;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public abstract class FlowAnalysis<N,A> {
	
	/**
	 * Analysis FlowGraph
	 */
	protected FlowGraph<N> graph;
	
	boolean isForward;
	
	/**
	 * Maps Node to IN-set
	 */
	protected  Map<N,A> nodeToINSet = new HashMap<N,A>();
	
	/**
	 * Maps Node to OUT-set
	 */
	protected  Map<N,A> nodeToOUTSet = new HashMap<N,A>();
	
	public FlowAnalysis(FlowGraph<N> fg, boolean isForward) { this.graph = fg; this.isForward = isForward; }
	
	/**
	 * Do flow analysis
	 */
	public void doAnalysis() {
		int roundCount = 0;
		int maxRounds = graph.size()*4;
		
		if(isForward) {
			//init startConfig
			Iterator<N> it = graph.iterator();
			while(it.hasNext()) {
				N node = it.next();
				nodeToINSet.put(node, getInitFlow());
				nodeToOUTSet.put(node, getInitFlow());
			}
			// set start
			N startNode = graph.getHead();
			nodeToINSet.put(startNode, getEntryInitFlow());
			
			//Do flow Analysis
			ArrayDeque<N> workQueue = new ArrayDeque<N> ();
			for(N node : graph.getForwardOrder()) {
				workQueue.add(node);
			}
			
			while(!workQueue.isEmpty() && maxRounds>roundCount) {
				N curNode = workQueue.pollFirst();
				
				// do IN-Set for this block 
				A inSet = nodeToINSet.get(curNode);
				
				boolean first=true;
				for(N before : graph.getPredsOf(curNode)) {
					if(first) {
						copy(nodeToOUTSet.get(before),inSet);
						first = false;
					}
					else
						mergeInto(nodeToOUTSet.get(before),inSet,curNode);
				}
				
				//do OUT-SET for this block
				A outSet = nodeToOUTSet.get(curNode);
				A buff = getInitFlow();
				copy(outSet,buff);
				flowThrough(inSet, curNode, outSet);
				
				if(!isEqual(buff,outSet)) {
					for(N succ : graph.getSuccsOf(curNode)) {
						if(!workQueue.contains(succ))
							workQueue.add(succ);
					}
				}
				roundCount++;
			}
		}
		else {
			//init startConfig
			Iterator<N> it = graph.iterator();
			while(it.hasNext()) {
				N node = it.next();
				nodeToINSet.put(node, getInitFlow());
				nodeToOUTSet.put(node, getInitFlow());
			}
			// set start
			N startNode = graph.getTail();
			nodeToINSet.put(startNode, getEntryInitFlow());
			
			//Do flow Analysis
			ArrayDeque<N> workQueue = new ArrayDeque<N> ();
			for(N node : graph.getBackwardOrder()) {
				workQueue.add(node);
			}

			
			while(!workQueue.isEmpty() && maxRounds>roundCount) {
				N curNode = workQueue.pollFirst();
				
				// do OUT-Set for this block 
				A outSet = nodeToOUTSet.get(curNode);
				
				boolean first=true;
				for(N before : graph.getSuccsOf(curNode)) {
					if(first) {
						copy(nodeToINSet.get(before),outSet);
						first = false;
					}
					else
						mergeInto(nodeToINSet.get(before),outSet,curNode);
				}
				
				//do IN-SET for this block
				A inSet = nodeToINSet.get(curNode);
				A buff = getInitFlow();
				copy(inSet,buff);
				flowThrough(outSet, curNode, inSet);
				
				if(!isEqual(buff,inSet)) {
					for(N pred : graph.getPredsOf(curNode)) {
						if(!workQueue.contains(pred))
							workQueue.add(pred);
					}
				}
				roundCount++;
			}
			
			if(maxRounds<=roundCount)
				throw new RuntimeException("No Fixpoint found");
		}
	}
	
	public A getINSet(N node) {
		return nodeToINSet.get(node);
	}
	
	public A getOUTSet(N node) {
		return nodeToOUTSet.get(node);
	}
	
	/**
	 * Transfer function
	 * @param in
	 * @param node
	 * @param out
	 */
	protected abstract void flowThrough(A in, N node, A out);
	
	protected abstract void mergeInto(A inSet, A out, N infFront);
	
	protected abstract A getEntryInitFlow();
	
	protected abstract A getInitFlow();
	
	protected abstract void copy(A src, A dest);
	
	protected abstract boolean isEqual(A a1, A a2);
	
	
}
