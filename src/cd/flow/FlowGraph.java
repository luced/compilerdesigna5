package cd.flow;

import java.util.Iterator;
import java.util.List;

public interface FlowGraph<N> extends Iterable<N> {
	
	 /** 
     *  Returns the start of the graph.
     */
    public N getHead();

    /** Returns the Last point*/
    public N getTail();

    /** 
     *  Returns a list of predecessors for the node.
     */
    public List<N> getPredsOf(N s);

    /**
     *  Returns a list of successors for the node.
     */
    public List<N> getSuccsOf(N s);

    /**
     *  Returns the node count for this graph.
     */
    public int size();

    /**
     *  Iterator of the Graph
     */
    public Iterator<N> iterator();
    
    public List<N> getForwardOrder();
    
    public List<N> getBackwardOrder();
	
}
